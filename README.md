# Ratio.py

Ratio.py it un outil en ligne de commande. Il envois des fausses données au tracker d'un torrent. 
Les émulateurs supportés:
* Transmission 2.92

## Dépendences:
1. Python 3.x
2. pip install -r requirements.txt

## Utilisation:
```console
user@computer:~/ratio.py$ python ratio.py -c configuration.json 
```

## Example de configuration
```js
{
   "torrent": "<Emplacement du torrent>",
   "upload": "<Vitesse d'envois (kB/s)>"
}
```
